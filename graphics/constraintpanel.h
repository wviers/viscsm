#ifndef CONSTRAINTPANEL_H
#define CONSTRAINTPANEL_H

#include <QWidget>
#include <QListWidget>
#include <QSpinBox>
#include "graph/nautygraph.h"

class FsgWindow;
namespace vcfsm {
namespace graphics {

class ConstraintPanel: public QWidget
{
public:
    ConstraintPanel(QWidget * parent);

private:
    QListWidget * m_constraints;
    QSpinBox * m_minsupport;
    FsgWindow * par;

public slots:
    void addConstraint(bool clicked = true);
    void filter(bool clicked=true);
    void augment(bool clicked=true);
};

}
}
#endif // CONSTRAINTPANEL_H
