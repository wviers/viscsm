//
// Created by squishy on 10/13/2016.
//

#ifndef VCFSM_COLORS_H
#define VCFSM_COLORS_H

#include <QColor>
#include <unordered_map>
#include <string>

namespace vcfsm {
namespace graphics {

const int NUM_COLORS = 22;
static const QColor kelly[] = {QColor(0xf2f3f4), QColor(0x222222), QColor(0xf3c300), QColor(0x875692),
                  QColor(0xf38400), QColor(0xa1caf1), QColor(0xbe0032), QColor(0xc2b280),
                  QColor(0x848482), QColor(0x008856), QColor(0xe68fac), QColor(0x0067a5),
                  QColor(0xf99379), QColor(0x604e97), QColor(0xf6a600), QColor(0xb3446c),
                  QColor(0xdcd300), QColor(0x882d17), QColor(0x8db600), QColor(0x654522),
                  QColor(0xe25822), QColor(0x2b3d26)};


}
}
#endif //VCFSM_COLORS_H
