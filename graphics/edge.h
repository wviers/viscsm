//
// Created by squishy on 10/11/2016.
//

#ifndef VCFSM_EDGE_H
#define VCFSM_EDGE_H

#include <QGraphicsItem>
#include <utility>
#include <functional>
#include "node.h"

namespace vcfsm {
namespace graphics {


class Edge: public QGraphicsItem
{
    public:
        Edge(Node * source, Node * dest);

        Node * sourceNode() const {return m_source;}
        Node * destNode() const {return m_dest;}

        void adjust();

        enum {Type = UserType + 2};
        int type() const Q_DECL_OVERRIDE {return Type;}

        bool operator == (const Edge & e) const;

    protected:
        QRectF boundingRect() const Q_DECL_OVERRIDE;
        void paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget) Q_DECL_OVERRIDE;

    private:
        Node * m_source, * m_dest;
        QPointF m_sourcePt;
        QPointF m_destPt;
        qreal arrowSize;
};



}
}

namespace std
{
template<> struct hash<vcfsm::graphics::Edge *>
{
    std::size_t combine(std::size_t t1, std::size_t t2) const
    {
        t1 ^= t2 + 0x9e3779b99e377800 + (t1 << 9) + (t1 >> 2);
        return t1;
    }

    std::size_t operator()(const vcfsm::graphics::Edge * e) const
    {
        hash<vcfsm::graphics::Node*> nodeHash;
        if (e->sourceNode() < e->destNode())
            return combine(nodeHash(e->sourceNode()), nodeHash(e->destNode()));
        return combine(nodeHash(e->destNode()), nodeHash(e->sourceNode()));
    }
};
}

#endif //VCFSM_EDGE_H
