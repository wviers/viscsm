#ifndef CONSTRAINTBUILDER_H
#define CONSTRAINTBUILDER_H
#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <vector>
#include <QListWidgetItem>
#include "constraintarg.h"
#include <QComboBox>
#include <graph/constraints.h>

Q_DECLARE_METATYPE(vcfsm::graph::constraint_t);

namespace vcfsm {
namespace graphics {


class ConstraintBuilder: public QDialog
{
public:
    ConstraintBuilder(QWidget * parent = Q_NULLPTR);
    static QListWidgetItem * getConstraint(QWidget * parent);

private:
    QVBoxLayout * m_layout;
    QVBoxLayout * argsLayout;
    QComboBox * constraint_type;
    std::vector<ConstraintArg * > args;
    static QListWidgetItem * item;

public slots:
    void switchConstraint(const QString & sel);
    void ok(const bool triggered = true);
    void cancel(const bool triggered = true);
};

}
}
#endif // CONSTRAINTBUILDER_H
