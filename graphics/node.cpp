//
// Created by squishy on 10/11/2016.
//

#include "node.h"
#include "edge.h"
#include "graphwidget.h"
#include "nodeproppanel.h"
#include "editwindow.h"
#include "graph/graphproperties.h"
#include "graph/SubGraphProperties.h"
#include <QGraphicsSceneMouseEvent>
#include <QtDebug>
#include <cmath>


namespace vcfsm{
namespace graphics{

Node::Node(GraphWidget * graphWidget): m_graph(graphWidget)
{
    setFlag(ItemIsMovable);
    setFlag(ItemSendsGeometryChanges);
    setCacheMode(DeviceCoordinateCache);
    setZValue(-1);
    graph::GraphProperties::nodeSet.insert(this);
}

//empty graphics node used for contraints, not drawn
Node::Node()
{
    setZValue(-1);
    graph::SubGraphProperties::nodeSet.insert(this);
}

void Node::addEdge(Edge *edge)
{
    m_edgeList << edge;
    edge->adjust();
}

QRectF Node::boundingRect() const
{
    qreal adjust = 2;
    // huh? what?
    // todo: look up what this does (and why)
    return QRectF(-10 - adjust, -10 - adjust, 23 + adjust, 23 + adjust);
}

QPainterPath Node::shape() const
{
    QPainterPath path;
    path.addEllipse(-10, -10, 20, 20);
    return path;
}

void Node::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *)
{
    //qDebug() << "Painting a node";
    painter->setPen(Qt::NoPen);
    painter->setBrush(Qt::darkGray);
    painter->drawEllipse(-7, -7, 20, 20);

    QRadialGradient gradient(-3, -3, 10);
    std::string label = vcfsm::graph::GraphProperties::label(this);
    auto it = m_graph->label_mapping.find(label);
    //qDebug() << m_graph->label_mapping.size();
    for (auto p:m_graph->label_mapping)
    {
        //qDebug() << QString::fromStdString(p.first) << p.second;
    }
    if (option->state & QStyle::State_Sunken) {
        gradient.setCenter(3, 3);
        gradient.setFocalPoint(3, 3);
        gradient.setColorAt(1, QColor(Qt::yellow).light(120));
        gradient.setColorAt(0, QColor(Qt::darkYellow).light(120));
    } else if (it != m_graph->label_mapping.end()) {
        //qDebug() << "Color is " << QString::fromStdString(label);
        gradient.setColorAt(0, it->second.lighter(150));
        gradient.setColorAt(1, it->second);
    } else {
        gradient.setColorAt(0, Qt::white);
        gradient.setColorAt(1, Qt::lightGray);
    }
    painter->setBrush(gradient);

    if (m_graph -> selectedNode == this)
        painter->setPen(QPen(Qt::blue, 2));
    else
        painter->setPen(QPen(Qt::black, 0));
    painter->drawEllipse(-10, -10, 20, 20);
}

QVariant Node::itemChange(GraphicsItemChange change, const QVariant &value)
{
    switch (change) {
        case ItemPositionHasChanged:
                    foreach (Edge *edge, m_edgeList)
                    edge->adjust();
            m_graph->itemMoved();
            break;
        default:
            break;
    };

    return QGraphicsItem::itemChange(change, value);
}

void Node::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    update();
    QGraphicsItem::mousePressEvent(event);
}

void Node::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_graph->currEdgeMode == GraphWidget::NONE)
    {
        m_graph->updateSelected(this);
        m_graph->par()->npp()->load(this);
    }
    else if (m_graph->par()->egp()!=nullptr)
    {
        if (m_graph->selectedNode == NULL)
        {
            m_graph->updateSelected(this);
            m_graph->par()->npp()->load(this);
        }
        else
        {
            Edge * e = new Edge(this, m_graph->selectedNode);
            if (graph::GraphProperties::edgeSet.find(e) == graph::GraphProperties::edgeSet.end())
            {
                graph::GraphProperties::edgeSet.insert(e);
                m_graph->scene()->addItem(new Edge(this, m_graph->selectedNode));
                if (m_graph->currEdgeMode == GraphWidget::SINGLE) {
                    m_graph->updateSelected(NULL);
                    m_graph->par()->egp()->resetAddEdgeBtn();
                }
                else if (m_graph->currEdgeMode == GraphWidget::PATH)
                    m_graph->updateSelected(this);
            }

        }
    }
    update();
    QGraphicsItem::mouseReleaseEvent(event);
}

void Node::calculateForces()
{
    if (!scene() || scene()->mouseGrabberItem() == this) {
        m_newPos = pos();
        return;
    }
    int numnodes = 0;
    // Sum up all forces pushing this item away
    qreal xvel = 0;
    qreal yvel = 0;
            foreach (QGraphicsItem *item, scene()->items()) {
            Node *node = qgraphicsitem_cast<Node *>(item);
            if (!node)
                continue;

            numnodes++;
            QPointF vec = mapToItem(node, 0, 0);
            qreal dx = vec.x();
            qreal dy = vec.y();
            if (dx == 0 && dy == 0 && node != this)
            {
                //perturb if two objects on top of each other
                dx = 10*(qrand() % 3 - 1);
                dy = 10*(qrand() % 3 - 1);
            }
            double l = 2.0 * (dx * dx + dy * dy);
            if (l > 0) {
                xvel += (dx * 150.0) / l;
                yvel += (dy * 150.0) / l;
            }
        }

    // Now subtract all forces pulling items together
    double weight = (m_edgeList.size() + 1) * 10;
            foreach (Edge *edge, m_edgeList) {
            QPointF vec;
            if (edge->sourceNode() == this)
                vec = mapToItem(edge->destNode(), 0, 0);
            else
                vec = mapToItem(edge->sourceNode(), 0, 0);
            xvel -= vec.x() / weight;
            yvel -= vec.y() / weight;
        }
    // ok, let's add gravity.  again.
    // kinda; we don't *need* to drop off quadratically, and this
    // maintains direction of the position vector
    double mass = 10;

    xvel -= pos().x()/mass;
    yvel -= pos().y()/mass;

    if (qAbs(xvel) < 0.1 && qAbs(yvel) < 0.1)
        xvel = yvel = 0;

    QRectF sceneRect = scene()->sceneRect();
    m_newPos = pos() + QPointF(xvel, yvel);
    m_newPos.setX(qMin(qMax(m_newPos.x(), sceneRect.left() + 10), sceneRect.right() - 10));
    m_newPos.setY(qMin(qMax(m_newPos.y(), sceneRect.top() + 10), sceneRect.bottom() - 10));
}

bool Node::advance()
{
    if (m_newPos == pos())
        return false;

    setPos(m_newPos);
    return true;
}


}
}
