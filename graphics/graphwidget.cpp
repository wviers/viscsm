//
// Created by squishy on 10/11/2016.
//

#include "graphwidget.h"
#include "nodeproppanel.h"
#include "node.h"
#include <QKeyEvent>
namespace vcfsm{
namespace graphics {

GraphWidget::GraphWidget(QWidget *parent): QGraphicsView(parent), timerID(0)
{
    m_scene = new QGraphicsScene(this);
    m_scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    //m_scene->setSceneRect(-200, -200, 400, 400);
    //m_scene->setSceneRect(0,0,this->width(), this->height());
    m_scene->setSceneRect(-this->width()/2,-this->height()/2, this->width(), this->height());
    m_parent = (GraphWindow*) parent;
    currEdgeMode = NONE;
    setScene(m_scene);
    setCacheMode(CacheBackground);
    setViewportUpdateMode(BoundingRectViewportUpdate);
    setRenderHint(QPainter::Antialiasing);
    setTransformationAnchor(AnchorUnderMouse);
    scale(qreal(0.8), qreal(0.8));
    setMinimumSize(400, 400);
    setWindowTitle(tr("Elastic Nodes"));
    selectedNode = nullptr;
    m_layout = false;
}

Node * GraphWidget::addNode()
{
    Node * node = new Node(this);
    node -> setPos(0, 0);
    m_scene -> addItem(node);
    return node;
}

void GraphWidget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
        default:
            QGraphicsView::keyPressEvent(event);
    }
}
/*
void GraphWidget::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect);

    selectedNode = NULL;
    // Shadow
    QRectF sceneRect = this->sceneRect();
    QRectF rightShadow(sceneRect.right(), sceneRect.top() + 5, 5, sceneRect.height());
    QRectF bottomShadow(sceneRect.left() + 5, sceneRect.bottom(), sceneRect.width(), 5);
    if (rightShadow.intersects(rect) || rightShadow.contains(rect))
        painter->fillRect(rightShadow, Qt::darkGray);
    if (bottomShadow.intersects(rect) || bottomShadow.contains(rect))
        painter->fillRect(bottomShadow, Qt::darkGray);

    // Fill
    QLinearGradient gradient(sceneRect.topLeft(), sceneRect.bottomRight());
    gradient.setColorAt(0, Qt::white);
    gradient.setColorAt(1, Qt::lightGray);
    painter->fillRect(rect.intersected(sceneRect), gradient);
    painter->setBrush(Qt::NoBrush);
    painter->drawRect(sceneRect);
}*/

void GraphWidget::itemMoved()
{
    if (!timerID)
    {
        timerID = startTimer(5);
    }
}

void GraphWidget::updateSelected(Node *sel)
{
    Node * temp = selectedNode;
    selectedNode = sel;
    if (temp)
        temp->update();
}

void GraphWidget::clear ()
{
    updateSelected(NULL);
    m_scene->clear();
    label_mapping.clear();
}

void GraphWidget::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);

    if (m_layout)
    {
        QList<Node *> nodes;
                foreach (QGraphicsItem *item, scene()->items())
            {
                if (Node *node = qgraphicsitem_cast<Node *>(item))
                    nodes << node;
            }

                foreach (Node *node, nodes)node->calculateForces();

        bool itemsMoved = false;
                foreach (Node *node, nodes)
            {
                if (node->advance())
                    itemsMoved = true;
            }

        if (!itemsMoved)
        {
            killTimer(timerID);
            timerID = 0;
            disableLayout();
        }
    }
}

void GraphWidget::forceLayout()
{
    if(timerID)
    {
        killTimer(timerID);
        timerID = 0;
    }
    enableLayout();
    itemMoved();
}

void GraphWidget::resizeEvent(QResizeEvent * event)
{
   m_scene->setSceneRect(-this->width()/2,-this->height()/2, this->width(), this->height());
   QGraphicsView::resizeEvent(event);
}

}
}
