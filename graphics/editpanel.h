//
// Created by squishy on 10/13/2016.
//

#ifndef VCFSM_EDITPANEL_H
#define VCFSM_EDITPANEL_H

#include <QWidget>
#include <QPushButton>
class EditWindow;
namespace vcfsm {
namespace graphics {

class EditPanel: public QWidget
{
    public:
        EditPanel(QWidget * parent=Q_NULLPTR);
        void resetAddEdgeBtn() {addEdge->setChecked(false);}
    private:
        QPushButton * addEdge, * addPath, * addStar;
        EditWindow * m_parent;
        EditWindow * par() {return m_parent;}

    private slots:
        void addNode();
        void singleEdgeMode(bool checked);
        void pathEdgeMode(bool checked);
        void starEdgeMode(bool checked);
};

}
}


#endif //VCFSM_EDITPANEL_H
