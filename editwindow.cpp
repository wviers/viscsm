//
// Created by squishy on 10/18/2016.
//

#include "editwindow.h"
#include "graph/graphstore.h"
#include "graph/graphproperties.h"
#include "graphics/fsgpanel.h"
#include <QDockWidget>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QFileDialog>

using namespace vcfsm;

EditWindow::EditWindow(QWidget *parent):GraphWindow(parent)
{
    graphWidget = new graphics::GraphWidget(this);
    editGraphPanel = new graphics::EditPanel(this);
    nodePropertyPanel = new graphics::NodePropPanel(this);
    QDockWidget * editGraphDock = new QDockWidget(tr("Graph Editor"), this);
    QDockWidget * nodePropertyDock = new QDockWidget(tr("Node Properties"), this);
    QDockWidget * fsgDock = new QDockWidget(tr("FSG Mode"), this);

    graphics::FsgPanel * fsgpanel = new graphics::FsgPanel(this);


    editGraphDock->setWidget(editGraphPanel);
    nodePropertyDock->setWidget(nodePropertyPanel);
    fsgDock->setWidget(fsgpanel);

    setCentralWidget(graphWidget);
    addDockWidget(Qt::RightDockWidgetArea, editGraphDock);
    addDockWidget(Qt::RightDockWidgetArea, nodePropertyDock);
    addDockWidget(Qt::RightDockWidgetArea, fsgDock);
    // setup the file menu
    QMenuBar * mb = new QMenuBar(this);
    QMenu * fileMenu = new QMenu("File", mb);
    QAction * openAct = fileMenu->addAction("Open");
    QAction * exportAct = fileMenu->addAction("Save");
    QAction * clearAct = fileMenu->addAction("Clear");

    openAct->setShortcut(QKeySequence::Open);
    exportAct->setShortcut(QKeySequence::Save);
    clearAct->setShortcut(QKeySequence::New);

    mb->addMenu(fileMenu);
    this->setMenuBar(mb);

    connect(exportAct, &QAction::triggered, this, &EditWindow::dumpGraph);
    connect(clearAct, &QAction::triggered, this, &EditWindow::clearGraph);
    connect(openAct, &QAction::triggered, this, &EditWindow::loadGraph);

}

void EditWindow::dumpGraph()
{
    QString fname = QFileDialog::getSaveFileName(this);
    graph::GraphStore::dumpGraph(fname.toStdString(), this);
}

void EditWindow::clearGraph()
{
    graphWidget->clear();
    graph::GraphProperties::edgeSet.clear();
    graph::GraphProperties::nodeSet.clear();
}

void EditWindow::loadGraph()
{
    QString fname = QFileDialog::getOpenFileName(this);
    graph::GraphStore::loadGraph(fname.toStdString(), this);
}
