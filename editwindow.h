//
// Created by squishy on 10/18/2016.
//

#ifndef VCFSM_EDITWINDOW_H
#define VCFSM_EDITWINDOW_H
#include "graphwindow.h"
#include "graphics/graphwidget.h"
#include "graphics/editpanel.h"
#include "graphics/nodeproppanel.h"
#include "graphics/node.h"
#include "graph/graphstore.h"

class EditWindow: public GraphWindow
{
    public:
        EditWindow(QWidget * parent = Q_NULLPTR);

    private:
        friend class vcfsm::graphics::GraphWidget;
        friend class vcfsm::graphics::EditPanel;
        friend class vcfsm::graphics::NodePropPanel;
        friend class vcfsm::graphics::Node;

    public slots:
        void dumpGraph();
        void clearGraph();
        void loadGraph();

};


#endif //VCFSM_EDITWINDOW_H
