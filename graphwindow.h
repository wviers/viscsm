//
// Created by squishy on 11/16/16.
//

#ifndef VCFSM_GRAPHWINDOW_H
#define VCFSM_GRAPHWINDOW_H
#include <QMainWindow>
#include "graphics/nodeproppanel.h"
#include "graphics/graphwidget.h"
#include "graphics/editpanel.h"
class GraphWindow: public QMainWindow
{
    public:
        GraphWindow(QWidget * parent = Q_NULLPTR):QMainWindow(parent){}
        vcfsm::graphics::GraphWidget * gw() {return graphWidget;}
        vcfsm::graphics::NodePropPanel * npp() {return nodePropertyPanel;}
        vcfsm::graphics::EditPanel * egp() {return editGraphPanel;}

    protected:
        vcfsm::graphics::NodePropPanel * nodePropertyPanel;
        vcfsm::graphics::GraphWidget * graphWidget;
        vcfsm::graphics::EditPanel * editGraphPanel;
};
#endif //VCFSM_GRAPHWINDOW_H
