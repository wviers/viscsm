#include "constraints.h"
#include "graphstore.h"
#include "freqsubgraphs.h"

#include <iostream>
#include <fstream>

#include "json.hpp"
using json = nlohmann::json;

namespace vcfsm {
namespace graph {

constraint_t Constraints::containsLabel(const std::vector<std::string> & args)
{
    std::string label = args[0];
    return [label](const NautyGraph & ng) {
        for (int n: ng.connNodes())
        {
            if (ng.labelOf(n) == label)
                return true;
        }
        return false;
    };
}

constraint_t Constraints::containsName(const std::vector<std::string> & args)
{
    std::string label = args[0];
    return [label](const NautyGraph & ng) {
        for (int n: ng.connNodes())
        {
            if (ng.nameOf(n) == label)
                return true;
        }
        return false;
    };
}

constraint_t Constraints::containsNumEdges(const std::vector<std::string> & args)
{
    int numEdges = std::stoi(args[0]);
    return [numEdges](const NautyGraph & ng) {
    for (int n: ng.connNodes())
    {
        if (ng.size() == numEdges)
            return true;
    }
            return false;
    };
}

constraint_t Constraints::excludesNumEdges(const std::vector<std::string> & args)
{
    int numEdges = std::stoi(args[0]);
    return [numEdges](const NautyGraph & ng)
    {
        for (int n: ng.connNodes())
        {
            if (ng.size() != numEdges)
                return true;
        }
        return false;
    };
}

constraint_t Constraints::excludeByLabelAppearance(const std::vector<std::string> & args)
{
    std::string label = args[0];
    int numAppearances = std::stoi(args[1]);

    return [numAppearances, label](const NautyGraph & ng)
    {
        int count = 0;
        for (int n: ng.connNodes())
        {
            if (ng.labelOf(n) == label)
            {
                count++;
            }
            if(count >= numAppearances)
            {
                //Break and exclude graph because label appearance has
                //exceeded threshold
                return false;
            }
        }
        return true;
    };
}

constraint_t Constraints::includeByVertexDegree(const std::vector<std::string> & args)
{
    int degree = std::stoi(args[0]);

    return [degree](const NautyGraph & ng)
    {
        for (int n: ng.connNodes())
        {
            if(ng.edgeDests(n).size() > degree)
            {
                //Break and include the graph because vertex degree appearance has
                //exceeded threshold
                return true;
            }
        }
        return false;
    };
}

constraint_t Constraints::includeByAverageVertexDegree(const std::vector<std::string> & args)
{
    double averageThreshold = (double) std::stod(args[0]);
    return [averageThreshold](const NautyGraph & ng)
    {
        double sum = 0;
        for (int n: ng.connNodes())
        {
            sum += (double) ng.edgeDests(n).size();
        }
        if(sum / ng.connNodes().size() > averageThreshold)
        {
            return true;
        }
        else
        {
            return false;
        }
    };
}

constraint_t Constraints::excludeSpecificVertexConnection(const std::vector<std::string> & args)
{
    std::string label = args[0];

    return [label](const NautyGraph & ng)
    {
        //for all nodes in sub graph
        for (int n: ng.connNodes())
        {
            //for all edges in graph
            for(graphics::Edge * ep: GraphProperties::edgeSetStash)
            {
                //if source is current node in subgraph and dest is exclude node then exclude
                if(GraphProperties::label(ep->sourceNode()) == ng.labelOf(n))
                {
                    if(GraphProperties::label(ep->destNode()) == label)
                    {
                        return false;
                    }
                }
                //if dest is current node in subgraph and source is exclude node then exclude
                if(GraphProperties::label(ep->destNode()) == ng.labelOf(n))
                {
                    if(GraphProperties::label(ep->sourceNode()) == label)
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    };
}

constraint_t Constraints::includeSpecificVertexConnection(const std::vector<std::string> & args)
{
    std::string label = args[0];

    return [label](const NautyGraph & ng)
    {
        //for all nodes in sub graph
        for (int n: ng.connNodes())
        {
            //for all edges in graph
            for(graphics::Edge * ep: GraphProperties::edgeSetStash)
            {
                //if source is current node in subgraph and dest is exclude node then exclude
                if(GraphProperties::label(ep->sourceNode()) == ng.labelOf(n))
                {
                    if(GraphProperties::label(ep->destNode()) == label)
                    {
                        return true;
                    }
                }
                //if dest is current node in subgraph and source is exclude node then exclude
                if(GraphProperties::label(ep->destNode()) == ng.labelOf(n))
                {
                    if(GraphProperties::label(ep->sourceNode()) == label)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    };
}

constraint_t Constraints::includesContainsSubgraph(const std::vector<std::string> & args)
{
    std::string path = args[0];
    return [path](const NautyGraph & ng)
    {
        Constraints con;
        con.loadGraph(path);

        //compare current graph
        std::unordered_set<const graphics::Edge *> edgesComparing = ng.GetGraphicalEdges();
        bool atLeastOneEdgeFound = false;
        bool foundEdgeThisRun = true; //Set to true here only to not set off failure condition at start of loop
        bool singleFailure = false;
        if (SubGraphProperties::nodeSet.size() == ng.connNodes().size())
        {
            //for all edges in graph we are looking for
            for (graphics::Edge *searchEdge: SubGraphProperties::edgeSet)
            {
                if (!foundEdgeThisRun)
                {
                    singleFailure = true;
                    break;
                }
                else
                    foundEdgeThisRun = false;

                //compare this edge to all the edges in the current graph
                for (const graphics::Edge *compareEdge: edgesComparing)
                {
                    if (SubGraphProperties::label(searchEdge->sourceNode()) ==
                        GraphProperties::label(compareEdge->sourceNode()))
                    {
                        if (SubGraphProperties::label(searchEdge->destNode()) ==
                            GraphProperties::label(compareEdge->destNode()))
                        {
                            foundEdgeThisRun = true;
                            atLeastOneEdgeFound = true;
                        }
                    }
                    if (SubGraphProperties::label(searchEdge->sourceNode()) ==
                        GraphProperties::label(compareEdge->destNode()))
                    {
                        if (SubGraphProperties::label(searchEdge->destNode()) ==
                            GraphProperties::label(compareEdge->sourceNode()))
                        {
                            foundEdgeThisRun = true;
                            atLeastOneEdgeFound = true;
                        }
                    }
                }
            }
            //check if last run found an edge
            if (!foundEdgeThisRun)
            {
                singleFailure = true;
            }
        }

        if (!singleFailure && atLeastOneEdgeFound)
        {
            //Stash subgraph edges read in from data file
            SubGraphProperties::stash();
            return true;
        }

        //else look if this subgraph is in the subgraphs of the current graph
        //for all the subgraphs of the current graph
        if (ng.size() > 1)
        {
            std::vector<NautyGraph> graphs = ng.GetSingleEdgeSubGraphs();

            std::shared_ptr<graph::FreqSubGraphs> fsgs(new graph::FreqSubGraphs(graphs));
            //this loop advances the subgraphs of the current graph to 1 edge short of where the
            //FSG has been advanced to
            for (int i = 0; i < GraphProperties::plusEdgesClicked - 1 && i < ng.size() - 1; i++)
                fsgs->augmentWithSpecificEdges(ng);
            std::vector<NautyGraph> subgraphsToLookThrough = fsgs->FSGs(1);

            //All subgraphs of current graph
            for (NautyGraph compareGraph: subgraphsToLookThrough)
            {
                std::unordered_set<const graphics::Edge *> subEdgesComparing = compareGraph.GetGraphicalEdges();
                atLeastOneEdgeFound = false;
                singleFailure = false;
                foundEdgeThisRun = true;
                if (SubGraphProperties::nodeSet.size() == compareGraph.connNodes().size())
                {
                    for (graphics::Edge *searchEdge: SubGraphProperties::edgeSet)
                    {
                        //for all edges in graph we are looking for
                        if (!foundEdgeThisRun)
                        {
                            //This edge not represented return false
                            singleFailure = true;
                            break;
                        }
                        else
                            foundEdgeThisRun = false;

                        //compare this edge to all the edges in the current sub graph
                        for (const graphics::Edge *compareEdge: subEdgesComparing)
                        {
                            if (SubGraphProperties::label(searchEdge->sourceNode()) ==
                                GraphProperties::label(compareEdge->sourceNode()))
                            {
                                if (SubGraphProperties::label(searchEdge->destNode()) ==
                                    GraphProperties::label(compareEdge->destNode()))
                                {
                                    foundEdgeThisRun = true;
                                    atLeastOneEdgeFound = true;
                                }
                            }
                            if (SubGraphProperties::label(searchEdge->sourceNode()) ==
                                GraphProperties::label(compareEdge->destNode()))
                            {
                                if (SubGraphProperties::label(searchEdge->destNode()) ==
                                    GraphProperties::label(compareEdge->sourceNode()))
                                {
                                    foundEdgeThisRun = true;
                                    atLeastOneEdgeFound = true;
                                }
                            }
                        }
                    }
                    if (!foundEdgeThisRun)
                    {
                        singleFailure = true;
                    }
                    if (!singleFailure && atLeastOneEdgeFound)
                    {
                        //Stash subgraph edges read in from data file
                        SubGraphProperties::stash();
                        return true;
                    }
                }
            }
        }
        //if we got to here and never found an edge it means the current graph has no subgraphs
        //of the same size as the target graph

        //Stash subgraph edges read in from data file
        SubGraphProperties::stash();
        return false;
    };
}

constraint_t Constraints::ExcludesContainsSubgraph(const std::vector<std::string> & args)
{
    std::string path = args[0];
    return [path](const NautyGraph &ng)
    {
        Constraints con;
        con.loadGraph(path);

        //compare current graph
        std::unordered_set<const graphics::Edge *> edgesComparing = ng.GetGraphicalEdges();
        bool atLeastOneEdgeFound = false;
        bool foundEdgeThisRun = true; //Set to true here only to not set off failure condition at start of loop
        bool singleFailure = false;
        if (SubGraphProperties::nodeSet.size() == ng.connNodes().size())
        {
            //for all edges in graph we are looking for
            for (graphics::Edge *searchEdge: SubGraphProperties::edgeSet)
            {
                if (!foundEdgeThisRun)
                {
                    singleFailure = true;
                    break;
                }
                else
                    foundEdgeThisRun = false;

                //compare this edge to all the edges in the current graph
                for (const graphics::Edge *compareEdge: edgesComparing)
                {
                    if (SubGraphProperties::label(searchEdge->sourceNode()) ==
                        GraphProperties::label(compareEdge->sourceNode()))
                    {
                        if (SubGraphProperties::label(searchEdge->destNode()) ==
                            GraphProperties::label(compareEdge->destNode()))
                        {
                            foundEdgeThisRun = true;
                            atLeastOneEdgeFound = true;
                        }
                    }
                    if (SubGraphProperties::label(searchEdge->sourceNode()) ==
                        GraphProperties::label(compareEdge->destNode()))
                    {
                        if (SubGraphProperties::label(searchEdge->destNode()) ==
                            GraphProperties::label(compareEdge->sourceNode()))
                        {
                            foundEdgeThisRun = true;
                            atLeastOneEdgeFound = true;
                        }
                    }
                }
            }
            //check if last run found an edge
            if (!foundEdgeThisRun)
            {
                singleFailure = true;
            }
        }

        if (!singleFailure && atLeastOneEdgeFound)
        {
            //Stash subgraph edges read in from data file
            SubGraphProperties::stash();
            return false;
        }

        //else look if this subgraph is in the subgraphs of the current graph
        //for all the subgraphs of the current graph
        if (ng.size() > 1)
        {
            std::vector<NautyGraph> graphs = ng.GetSingleEdgeSubGraphs();

            std::shared_ptr<graph::FreqSubGraphs> fsgs(new graph::FreqSubGraphs(graphs));
            //this loop advances the subgraphs of the current graph to 1 edge short of where the
            //FSG has been advanced to
            for (int i = 0; i < GraphProperties::plusEdgesClicked - 1 && i < ng.size() - 1; i++)
                fsgs->augmentWithSpecificEdges(ng);
            std::vector<NautyGraph> subgraphsToLookThrough = fsgs->FSGs(1);

            //All subgraphs of current graph
            for (NautyGraph compareGraph: subgraphsToLookThrough)
            {
                std::unordered_set<const graphics::Edge *> subEdgesComparing = compareGraph.GetGraphicalEdges();
                atLeastOneEdgeFound = false;
                singleFailure = false;
                foundEdgeThisRun = true;
                if (SubGraphProperties::nodeSet.size() == compareGraph.connNodes().size())
                {
                    for (graphics::Edge *searchEdge: SubGraphProperties::edgeSet)
                    {
                        //for all edges in graph we are looking for
                        if (!foundEdgeThisRun)
                        {
                            //This edge not represented return false
                            singleFailure = true;
                            break;
                        }
                        else
                            foundEdgeThisRun = false;

                        //compare this edge to all the edges in the current sub graph
                        for (const graphics::Edge *compareEdge: subEdgesComparing)
                        {
                            if (SubGraphProperties::label(searchEdge->sourceNode()) ==
                                GraphProperties::label(compareEdge->sourceNode()))
                            {
                                if (SubGraphProperties::label(searchEdge->destNode()) ==
                                    GraphProperties::label(compareEdge->destNode()))
                                {
                                    foundEdgeThisRun = true;
                                    atLeastOneEdgeFound = true;
                                }
                            }
                            if (SubGraphProperties::label(searchEdge->sourceNode()) ==
                                GraphProperties::label(compareEdge->destNode()))
                            {
                                if (SubGraphProperties::label(searchEdge->destNode()) ==
                                    GraphProperties::label(compareEdge->sourceNode()))
                                {
                                    foundEdgeThisRun = true;
                                    atLeastOneEdgeFound = true;
                                }
                            }
                        }
                    }
                    if (!foundEdgeThisRun)
                    {
                        singleFailure = true;
                    }
                    if (!singleFailure && atLeastOneEdgeFound)
                    {
                        //Stash subgraph edges read in from data file
                        SubGraphProperties::stash();
                        return false;
                    }
                }
            }
        }
        //if we got to here and never found an edge it means the current graph has no subgraphs
        //of the same size as the target graph

        //Stash subgraph edges read in from data file
        SubGraphProperties::stash();
        return true;
    };
}

const double TAU = 6.283185307;
void Constraints::loadGraph(const std::string filename)
{
    std::ifstream gfile(filename.c_str());
    json graph;
    gfile >> graph;
    gfile.close();

    std::map<nlohmann::basic_json<>::value_type, pt2d> nodes;
    std::map<nlohmann::basic_json<>::value_type, graphics::Node *> graphNodes;

    double angle = TAU / graph["nodes"].size();
    double rad = 100.0;
    auto xpos = [angle, rad](int n) { return rad * std::cos(angle * n); };
    auto ypos = [angle, rad](int n) { return rad * std::sin(angle * n); };
    for (int i = 0; i < graph["nodes"].size(); i++)
        nodes[graph["nodes"][i]["id"]] = {xpos(i), ypos(i)};

    for (int i = 0; i < graph["nodes"].size(); i++)
    {
        //won't be drawing this don't need a widget ptr
        graphics::Node *n = new graphics::Node();
        n->setPos({nodes[graph["nodes"][i]["id"]].x, nodes[graph["nodes"][i]["id"]].y});
        graphNodes[graph["nodes"][i]["id"]] = n;
        graph::SubGraphProperties::nodeSet.insert(n);
        if (graph["nodes"][i].find("name") != graph["nodes"][i].end())
            graph::SubGraphProperties::setName(n, graph["nodes"][i]["name"]);
        if (graph["nodes"][i].find("label") != graph["nodes"][i].end())
        {
            std::string label = graph["nodes"][i]["label"];
            graph::SubGraphProperties::setLabel(n, label);
        }
    }

    for (int i = 0; i < graph["edges"].size(); i++)
    {
        graphics::Edge *e = new graphics::Edge(graphNodes[graph["edges"][i][0]], graphNodes[graph["edges"][i][1]]);
        graph::SubGraphProperties::edgeSet.insert(e);
    }
}


const std::unordered_map<std::string, std::pair<generator_t, std::vector<std::string>>> Constraints::generators = {
        {"Contains label", {containsLabel, {"Label"}}},
        {"Contains name", {containsName, {"Name"}}},
        {"Includes graphs with specified number of edges", {containsNumEdges, {"Number of Edges"}}},
        {"Excludes graphs with number of edges", {excludesNumEdges, {"Number of Edges"}}},
        {"Excludes graphs by label appearance", {excludeByLabelAppearance, {"Label", "Number Of Appearances"}}},
        {"Includes graphs with a vertex greater than degree", {includeByVertexDegree, {"Vertex Degree"}}},
        {"Includes graphs with an average vertex greater than degree", {includeByAverageVertexDegree, {"Average Vertex Degree"}}},
        {"Includes graphs that are connected to a vertice with the specified label", {includeSpecificVertexConnection, {"Label"}}},
        {"Excludes graphs that are connected to a vertice with the specified label", {excludeSpecificVertexConnection, {"Label"}}},
        {"Includes graphs that contain a specific subgraph", {includesContainsSubgraph, {"Path to file containing connected graph"}}},
        {"Excludes graphs that contain a specific subgraph", {ExcludesContainsSubgraph, {"Path to file containing connected graph"}}}
    };
}
}
