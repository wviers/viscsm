#ifndef CONSTRAINTS_H
#define CONSTRAINTS_H

#include "nautygraph.h"
//#include "NautySubGraph.h"
#include "graphproperties.h"
#include "SubGraphProperties.h"

#include <functional>
#include <nauty/nausparse.h>
namespace vcfsm {
namespace graph {

typedef std::function<bool (const NautyGraph&)> constraint_t;
typedef std::function<constraint_t (const std::vector<std::string>&)> generator_t;

class Constraints
{
public:
    static constraint_t containsLabel(const std::vector<std::string> & args);
    static constraint_t containsName(const std::vector<std::string> & args);
    static constraint_t containsNumEdges(const std::vector<std::string> & args);
    static constraint_t excludesNumEdges(const std::vector<std::string> & args);
    static constraint_t excludeByLabelAppearance(const std::vector<std::string> & args);
    static constraint_t includeByVertexDegree(const std::vector<std::string> & args);
    static constraint_t includeByAverageVertexDegree(const std::vector<std::string> & args);
    static constraint_t includeSpecificVertexConnection(const std::vector<std::string> & args);
    static constraint_t excludeSpecificVertexConnection(const std::vector<std::string> & args);
    static constraint_t includesContainsSubgraph(const std::vector<std::string> & args);
    static constraint_t ExcludesContainsSubgraph(const std::vector<std::string> & args);

    static void loadGraph(const std::string filename);

    const static std::unordered_map<std::string, std::pair<generator_t, std::vector<std::string>>> generators;
};


}
}
#endif // CONSTRAINTS_H
