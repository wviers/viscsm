//
// Created by squishy on 11/12/16.
//

#ifndef VCFSM_FSG_H
#define VCFSM_FSG_H

#include "nautygraph.h"
#include "constraints.h"
#include <unordered_map>
#include <unordered_map>
#include <nauty/nausparse.h>

inline bool operator == (const sparsegraph & a, const sparsegraph & b)
{
    if (a.nv != b.nv || a.nde != b.nde)
        return false;
    for (int i=0; i<a.nv; i++)
    {
        if (a.d[i] != b.d[i])
            return false;

    }
    return true;
}

namespace vcfsm {
namespace graph {

typedef std::unordered_map<sparsegraph, std::unordered_set<NautyGraph>> gBucketer;

class FreqSubGraphs
{
    public:
        FreqSubGraphs();
        FreqSubGraphs(std::vector<NautyGraph> oneEdges);
        void augmentSubgraphs();
        void augmentWithSpecificEdges(const NautyGraph edges);
        std::vector<NautyGraph> FSGs(const int threshold = 2) const;
        std::vector<NautyGraph> FSGs(const std::vector<constraint_t> constraints, const int threshold = 2) const;
    private:
        void augmentAndAdd(const NautyGraph & g);
        void augmentAndAddSpecific(const NautyGraph& edgesToAddToo, const NautyGraph& EdgesToAdd);
        gBucketer canonicalSubgraphs;
};

}
}

#endif //VCFSM_FSG_H
