//
// Created by squishy on 10/13/2016.
//

#include "SubGraphProperties.h"

namespace vcfsm {
namespace graph {

std::unordered_map<const graphics::Node*, std::string> SubGraphProperties::m_names = std::unordered_map<const graphics::Node*, std::string>();
std::unordered_map<const graphics::Node*, std::string> SubGraphProperties::m_labels = std::unordered_map<const graphics::Node*, std::string>();

std::unordered_set<graphics::Edge*> SubGraphProperties::edgeSet = std::unordered_set<graphics::Edge*>();
std::unordered_set<graphics::Edge*> SubGraphProperties::edgeSetStash = std::unordered_set<graphics::Edge*>();
std::unordered_set<graphics::Node*> SubGraphProperties::nodeSet = std::unordered_set<graphics::Node*>();
std::unordered_set<graphics::Node*> SubGraphProperties::nodeSetStash = std::unordered_set<graphics::Node*>();

std::string SubGraphProperties::defaultGet(std::unordered_map<const graphics::Node *, std::string> &map,
                                                const graphics::Node *key)
{
    auto it = map.find(key);
    if (it == map.end())
        return "";
    return it->second;
}
std::string SubGraphProperties::name(const graphics::Node * node)
{
    return defaultGet(m_names, node);
}

std::string SubGraphProperties::label(const graphics::Node *node)
{
    return defaultGet(m_labels, node);
}

void SubGraphProperties::stash()
{
    edgeSetStash = std::move(edgeSet);
    nodeSetStash = std::move(nodeSet);
    edgeSet = std::unordered_set<graphics::Edge *>();
    nodeSet = std::unordered_set<graphics::Node *>();
}

void SubGraphProperties::unstash()
{
    edgeSet = std::move(edgeSetStash);
    nodeSet = std::move(nodeSetStash);
    edgeSetStash = std::unordered_set<graphics::Edge *>();
    nodeSetStash = std::unordered_set<graphics::Node *>();
}

}
}
