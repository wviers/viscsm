//
// Created by squishy on 11/16/16.
//

#ifndef VCFSM_FSGWINDOW_H
#define VCFSM_FSGWINDOW_H

#include "graphwindow.h"
#include <QColor>
#include <QCloseEvent>
#include "graphics/graphwidget.h"
#include "graphics/nodeproppanel.h"
#include "graph/nautygraph.h"
#include "editwindow.h"
#include "graphics/constraintpanel.h"
#include "graph/freqsubgraphs.h"
#include <memory>

class FsgWindow: public GraphWindow
{
    public:
        FsgWindow(QWidget * parent = Q_NULLPTR);
        void setLabelMaps(const std::unordered_map<std::string, QColor> & labels);
        void drawGraphs(const std::vector<vcfsm::graph::NautyGraph> & graphs);

        void closeEvent(QCloseEvent * event);
        std::shared_ptr<vcfsm::graph::FreqSubGraphs> fsgs;


    private:
        void drawGraph(const vcfsm::graph::NautyGraph & ng);
        EditWindow * callback;

        vcfsm::graphics::Node * getMapping(std::unordered_map<const vcfsm::graphics::Node *, vcfsm::graphics::Node *> & mapping,
                                           const vcfsm::graphics::Node * n);
        vcfsm::graphics::ConstraintPanel * constraintPanel;

};


#endif //VCFSM_FSGWINDOW_H
